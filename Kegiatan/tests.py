from django.test import TestCase, Client,RequestFactory
from django.urls import reverse, resolve
from .models import Kegiatan,Peserta
from . import views
from . import models

class Tests(TestCase):
    def setUp(self):
        Kegiatan.objects.create(nama_kegiatan="Gas")
        Peserta.objects.create(nama_peserta="kimus",kegiatan=Kegiatan.objects.get(nama_kegiatan="Gas"))

    def test_list_url_is_resolved(self):
        response = Client().get('/kegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved2(self):
        response = Client().get('/kegiatan/tambahpeserta')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved3(self):
        response = Client().get('/kegiatan/tambahkegiatan')
        self.assertEquals(response.status_code, 200)

    def test_input_tombol_add(self):
        response = Client().get('/kegiatan/')
        html = response.content.decode('utf-8')
        self.assertIn('Tambah Peserta', html)

    def test_input_tombol_add2(self):
        response = Client().get('/kegiatan/tambahpeserta')
        html = response.content.decode('utf-8')
        self.assertIn('Tambah', html)

    def test_input_tombol_add3(self):
        response = Client().get('/kegiatan/tambahkegiatan')
        html = response.content.decode('utf-8')
        self.assertIn('Tambah', html)
