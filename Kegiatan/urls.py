from django.urls import path

from .views import list_kegiatan,tambah_peserta,tambah_kegiatan

app_name = 'Kegiatan'


urlpatterns = [

    path('', list_kegiatan , name = 'list'),
    path('tambahpeserta', tambah_peserta, name = 'tambahpeserta'),
    path('tambahkegiatan', tambah_kegiatan, name = 'tambahkegiatan'),
]